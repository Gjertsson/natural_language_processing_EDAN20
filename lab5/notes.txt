Prio 1: Right arc
Prio 2: Left arc
Prio 3: Reduce
Prio 4: Shift

Questions:

############

Given a manually-annotated dependency graph, what are the conditions on the stack and the current input list -- the queue -- to execute left-arc, right-arc, shift, or reduce? Start with left-arc and right-arc, which are the simplest ones. 

Answer:
left-arc: stack must exist and the head of the first element on the queue is the first element on the stack
right-arc: stack must exist and the head of the first element on the stack is the first element on the queue
reduce: if the element at the top of the stack has already been processed/added to the state variable
shift: if no other options are available


############

The parser can only deal with projective sentences. In the case of a nonprojective one, the parsed graph and the manually-annotated sentence are not equal. Examine one such sentence and explain why it is not projective. Take a short one (the shortest). 

Answer:
man kan inte gå överallt

############

Nivre's parser sets constraints to actions. Name a way to encode these constraints as features. Think of Boolean features. 

Answer: 
Booleans, e.g. "can do left arc"

############

