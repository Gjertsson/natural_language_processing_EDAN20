"""
Gold standard parser
"""
__author__ = "Pierre Nugues"

import transition
import conll
import features

from sklearn.feature_extraction import DictVectorizer
from sklearn import linear_model
from sklearn.model_selection import cross_val_score

from sklearn.externals import joblib


def encode_classes(y_symbols):
    # We extract the chunk names
    classes = sorted(list(set(y_symbols)))
    # We assign each name a number
    dict_classes = dict(enumerate(classes))

    # We build an inverted dictionary
    inv_dict_classes = {v: k for k, v in dict_classes.items()}

    # We convert y_symbols into a numerical vector
    y = [inv_dict_classes[i] for i in y_symbols]
    return y, dict_classes, inv_dict_classes


def reference(stack, queue, state):
    """
    Gold standard parsing
    Produces a sequence of transitions from a manually-annotated corpus:
    sh, re, ra.deprel, la.deprel
    :param stack: The stack
    :param queue: The input list
    :param state: The set of relations already parsed
    :return: the transition and the grammatical function (deprel) in the
    form of transition.deprel
    """

#    print(queue[0])    
    
    # Right arc
    if stack and stack[0]['id'] == queue[0]['head']:
#        print('ra', queue[0]['deprel'], stack[0]['cpostag'], queue[0]['cpostag'])
        deprel = '.' + queue[0]['deprel']
        stack, queue, state = transition.right_arc(stack, queue, state)
#        print('ra')
        return stack, queue, state, 'ra' + deprel
        
    # Left arc
    if stack and queue[0]['id'] == stack[0]['head']:
#        print('la', stack[0]['deprel'], stack[0]['cpostag'], queue[0]['cpostag'])
        deprel = '.' + stack[0]['deprel']
        stack, queue, state = transition.left_arc(stack, queue, state)
#        print('la')
        return stack, queue, state, 'la' + deprel
        
    # Reduce
    if stack and transition.can_reduce(stack, state):
        for word in stack:
            if (word['id'] == queue[0]['head'] or
                        word['head'] == queue[0]['id']):
#                print('re')
#                print('re', stack[0]['cpostag'], queue[0]['cpostag'])
                stack, queue, state = transition.reduce(stack, queue, state)
                return stack, queue, state, 're'
    # Shift
#    print('sh')
#    print('sh', [], queue[0]['cpostag'])
    stack, queue, state = transition.shift(stack, queue, state)
    return stack, queue, state, 'sh'


if __name__ == '__main__':
    train_file = 'data/swedish_talbanken05_train.conll'
    test_file = 'data/swedish_talbanken05_test_blind.conll'
    column_names_2006 = ['id', 'form', 'lemma', 'cpostag', 'postag', 'feats', 'head', 'deprel', 'phead', 'pdeprel']
    column_names_2006_test = ['id', 'form', 'lemma', 'cpostag', 'postag', 'feats']

    sentences = conll.read_sentences(train_file)
    formatted_corpus = conll.split_rows(sentences, column_names_2006)
    
#    feature_names = ['word_stack', 'pos_stack', 'word_queue',
#                     'pos_queue', 'can_do_left', 'can_do_reduce']
#    feature_names = ['pos_stack_0', 'word_stack_0', 'pos_stack_1', 
#        'word_stack_1', 'pos_queue_0', 'word_queue_0', 'pos_queue_1', 
#        'word_queue_1', 'can_do_reduce', 'can_do_left']
    feature_names = ['pos_stack_0', 'word_stack_0', 'pos_stack_1', 
        'word_stack_1', 'pos_queue_0', 'word_queue_0', 'pos_queue_1', 
        'word_queue_1', 'can_do_reduce', 'can_do_left', 'sentence_word',
        'sentence_pos', 'sentence_word_n1', 'sentence_pos_n1']
        
    sent_cnt = 0
    X_dict = list()
    y_symbols = list()
    for sentence in formatted_corpus:
        sent_cnt += 1
        if sent_cnt % 1000 == 0:
            print(sent_cnt, 'sentences on', len(formatted_corpus), flush=True)
        stack = []
        queue = list(sentence)
        state = {}
        state['heads'] = {}
        state['heads']['0'] = '0'
        state['deprels'] = {}
        state['deprels']['0'] = 'ROOT'
        
        while queue:
            feature = features.extract_third(stack, queue, state, feature_names, sentence)
            stack, queue, state, trans = reference(stack, queue, state)    
            X_dict.append(dict(zip(feature_names, feature)))
            y_symbols.append(trans)
            
            
        stack, state = transition.empty_stack(stack, state)
        
        # Poorman's projectivization to have well-formed graphs.
        for word in sentence:
            word['head'] = state['heads'][word['id']]
        
    
#    for i in range(len(X_dict)):
#        print('x =', X_dict[i], 'y =', y_symbols[i])
#        print('################')

    # machine learning stuff
    print('... doing machine learning stuff :)')
    vec = DictVectorizer(sparse=True)
    X = vec.fit_transform(X_dict)
    y, dict_classes, inv_dict_classes = encode_classes(y_symbols)
    
    classifier = linear_model.LogisticRegression(penalty='l2', dual=True, solver='liblinear', verbose=True)
    model = classifier.fit(X, y)
    print(model)
    
    scores = cross_val_score(classifier, X, y, cv=10)
    print(scores)
    
    joblib.dump(model, 'classifier_three.pkl', compress=9)

