# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 08:56:11 2016

@author: Konrad Gjerttsson and Karl Nilsson
"""
import sys
import regex as re
import math

def count_ngrams(words, n):
    ngrams = [tuple(words[inx:inx + n])
              for inx in range(len(words) - n + 1)]
    # "\t".join(words[inx:inx + n])
    frequencies = {}
    for ngram in ngrams:
        if ngram in frequencies:
            frequencies[ngram] += 1
        else:
            frequencies[ngram] = 1
    return frequencies

def print_unigram_data(formatted_lines, prob):
    print()
    print("Unigram Model")
    print("=====================================================")
    print("wi\tC(wi)\t#words\tP(wi)")
    print("=====================================================")
    for i in formatted_lines:
        print(i)
    print("=====================================================")
    print("Prob. unigrams:", prob)
    entr = abs(math.log(prob, 2))/10
    print("Entropy rate:", entr)
    print("Perplexity:", pow(2, entr))
    print()
    
def print_bigram_data(formatted_lines, prob):
    print()
    print("Bigram Model")
    print("=====================================================")        
    print("wi\twi+1\tCi,i+1\tC(i)\tP(wi+1|wi)")
    print("=====================================================")
    for i in formatted_lines:
        print(i)
    print("=====================================================")
    print("Prob. bigrams:", prob)
    entr = abs(math.log(prob, 2))/10
    print("Entropy rate:", entr)
    print("Perplexity:", pow(2, entr))
    print()
    
def unigram_prob(index):
    # the sentance
    arg = sys.argv[1]
    file = open(arg)
    text = file.read().lower()
    sentences = re.findall(r'[<s>].+[<\/s>]+', text)
    
    # unigram/bigram info    
    info = sys.stdin.read().lower()
    # extract relevant data from string
    data = re.findall(r'\P{L}+\s\P{N}+', info)    
    
    ngram_count = {}
    for line in data:
        number, name = line.split('\t')    
        number = re.sub('\s', '', number)
        name = re.findall(r'[a-zåäöêA-ZÅÄÖ]+', name)
        ngram_count[name[0]] = int(number)
        
    # calculate sentance probability
    nbr_words = 0
    for key, value in ngram_count.items():
        nbr_words += value        
    
    words = re.split('\s', sentences[index])
    for i in range(len(words)):
        words[i] = re.sub(r'\.', '', words[i])
        words[i] = re.sub(r'\,', '', words[i])
        words[i] = re.sub(r'\?', '', words[i])
        if words[i] == "<s>" or words[i] == "</s>":
            words[i] = "s"
            

    prob = 1.0
    formatted_lines = []
    count = 0
    for word in words:
        current_prob = ngram_count[word] / nbr_words
        w = word
        if w == "s":
            if count == 0:
                w = "<s>"
                count += 1
            else:
                w = "</s>"
                
        formatted_lines.append(w + "\t" + str(ngram_count[word]) + "\t" + str(nbr_words) + "\t" + str(current_prob))
        prob *= current_prob
        
    print_unigram_data(formatted_lines, prob)

def bigram_prob(index):
    # the sentance
    arg = sys.argv[1]
    file = open(arg)
    text = file.read().lower()
    sentences = re.findall(r'[<s>].+[<\/s>]+', text)
    
    # unigram/bigram info    
    info = sys.stdin.read().lower()
    # extract relevant data from string
    data = re.findall(r'\P{L}+\s\P{N}+', info)
    
    # extract bigram information
    ngram_count = {}
    for line in data:
        number, name = line.split('\t') 
        names_list = re.findall(r'\p{L}+', name) 
        names = (names_list[0], names_list[1])
        number = re.sub('\s', '', number)
        ngram_count[names] = int(number)
        
        
    arg_temp = sys.argv[3]
    file_temp = open(arg_temp)
    text_temp = file_temp.read()
    data_temp = re.findall(r'\P{L}+\s\P{N}+', text_temp)
    
    # extract onegram information
    one_gram_count = {}
    for line in data_temp:
        number, name = line.split('\t')    
        number = re.sub('\s', '', number)
        name = re.findall(r'[a-zåäöêA-ZÅÄÖ]+', name)
        one_gram_count[name[0]] = int(number)
    
    # count total number of words in corpus
    nbr_words = 0
    for key, value in one_gram_count.items():
        nbr_words += value
    
    # reformat <s>
    words = re.split('\s', sentences[index])
    for i in range(len(words)):
        words[i] = re.sub(r'\.', '', words[i])
        words[i] = re.sub(r'\,', '', words[i])
        if words[i] == "<s>" or words[i] == "</s>":
            words[i] = "s"

    prob = 1.0
    formatted_lines = []
    for i in range(len(words) - 1):
        current_prob = 0
        if (words[i], words[i+1]) in ngram_count:
            current_prob = ngram_count[(words[i], words[i+1])] / one_gram_count[words[i]]
        else:
            current_prob = one_gram_count[words[i+1]] / nbr_words
        
        w1 = words[i]
        w2 = words[i+1]
        if w1 == "s":
            w1 = "<s>"
        elif w2 == "s":
            w2 = "</s>"
        
        bgram_count = 0
        if (words[i], words[i+1]) in ngram_count:
            bgram_count = ngram_count[(words[i], words[i+1])]
        
        formatted_lines.append(w1 + "\t" + w2 + "\t" + str(bgram_count) + "\t" + str(one_gram_count[words[i]]) + "\t" + str(current_prob) )
        prob *= current_prob
        
    print_bigram_data(formatted_lines, prob)
    


if __name__=="__main__":
    index = 0
    if sys.argv[2] == "unigram":
        unigram_prob(index)
    elif sys.argv[2] == "bigram":
        bigram_prob(index)
    