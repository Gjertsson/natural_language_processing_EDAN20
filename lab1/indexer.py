# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# importing important stuff
import sys, re, pickle, os, math
from sklearn.metrics.pairwise import cosine_similarity

def get_files(dir, suffix):
    """
    Returns all the files in a folder ending with suffix
    
    :param dir:
    :param suffix:
    :return: the list of file names
    """
    files = []
    for file in os.listdir(dir):
        if file.endswith(suffix):
            files.append(file)
    return files

def find_words(file_name):
  # save words and positions in dict
    book = open(file_name, 'r').read()
    word_iter = re.finditer(r'[a-zåäö]+', book.lower())
    word_dict = dict()
    
    word_counter = 0
    for word in word_iter:
        word_counter += 1
        if word.group(0) in word_dict:
            word_dict[word.group(0)].append(word.start())
        else:
            word_dict[word.group(0)] = [word.start()]
    
    # write dict to file
    pickle.dump(word_dict, open(file_name.split(".")[0] + ".idx", "wb"))
    return word_counter
    
def load_pickles(folder_name):
    pickles = []
    for file_name in get_files(folder_name, ".idx"):
         pickles.append((pickle.load(open(folder_name + "/" + file_name, "rb")),
                         file_name.split(".")[0] + ".txt"))
    return pickles
    
def master_index(dics):
    master_index = {}
    for dic, name in dics:
        for key, values in dic.items():
            if key in master_index:
                master_index[key][name] = values
            else:
                master_index[key] = {}
                master_index[key][name] = values
    return master_index
    
#def compare_dics(dic1, dic2, word_set):
#    cosine_dic1 = []
#    cosine_dic2 = []
#    for word in word_set:
#        if word in dic1:
#            cosine_dic1.append(len(dic1[word]))
#            if word in dic2:
#                cosine_dic2.append(len(dic2[word]))
#            else:
#                cosine_dic2.append(0)
#        elif word in dic2:
#            cosine_dic1.append(0)
#            cosine_dic2.append(len(dic2[word]))
#    return cosine_sim(cosine_dic1, cosine_dic2)
    
def compare_dics(dic1_name, dic2_name, dic1, dic2, word_set, final_dic):
    cosine_dic1 = []
    cosine_dic2 = []
#    print(final_dic[dic1_name])
    for word in word_set:
        if word in dic1:
#            cosine_dic1.append(len(dic1[word]))
            cosine_dic1.append(final_dic[dic1_name][word])
            if word in dic2:
#                cosine_dic2.append(len(dic2[word]))
                cosine_dic2.append(final_dic[dic2_name][word])
            else:
                cosine_dic2.append(0)
        elif word in dic2:
            cosine_dic1.append(0)
#            cosine_dic2.append(len(dic2[word]))
            cosine_dic2.append(final_dic[dic2_name][word])
    return cosine_sim(cosine_dic1, cosine_dic2)
    
def cosine_sim(dic1, dic2):
    AB = 0    
    A_square = 0
    B_square = 0
    for i in range(len(dic1)):
        AB += dic1[i]*dic2[i]
        A_square += (dic1[i]*dic1[i])
        B_square += (dic2[i]*dic2[i])
    A_square = math.sqrt(A_square)    
    B_square = math.sqrt(B_square)
    cos_similarity = AB / (A_square * B_square)
    return cos_similarity
    

if __name__ == "__main__":
    # error handling
    if len(sys.argv) < 2:
        print('Too few arguments, arguments should match indexer.py folder_name')
        sys.exit()
    elif len(sys.argv) > 2:
        print('Too many arguments, arguments should match indexer.py folder_name')
        sys.exit()
    else:
        # store number of words in each document
        word_counts = {}
        for file_name in get_files(sys.argv[1], ".txt"):
            word_counts[file_name] = find_words(sys.argv[1] + "/" + file_name)
        
        dics = load_pickles(sys.argv[1])
        mindex = master_index(dics)
        pickle.dump(mindex, open("master_index.idx", "wb"))
        
        final_dic = {}
        word_set = set()
        nbr_docs = len(dics)
        
        # dics = [(dictionary, file_name)]
        for dic in dics:
            tidf_dic = {}
            name = dic[1]
            # key = word, value = list of occurances
            for key, value in dic[0].items():
                tf = len(value) / float(word_counts[name])
                idf = math.log(len(word_counts) / float(len(mindex[key])), 10)
                tidf_dic[key] = tf * idf
                word_set.add(key)
            
            final_dic[name] = tidf_dic
            
        for word in word_set:
            for key in final_dic:
                if word not in final_dic[key]:
                    final_dic[key][word] = 0.0
        
        pickle.dump(final_dic, open("final_dictionary.idx", "wb"))
            
            
        # compare documents
        comparation = []        
        for i in range(len(dics) - 1):
            for j in range(i + 1, len(dics)):
                comparation.append((dics[i][1] + " - " + dics[j][1], 
                               compare_dics(dics[i][1], dics[j][1], dics[i][0], 
                                            dics[j][0], word_set, final_dic)))
                               
        for comp in comparation:
            print(comp[0], comp[1])
            
#        for key, value in final_dic.items():
#            print(key)
#            for word, number in value.items():
#                if word == 'känna' or word == 'gås' or word == 'nils' or word == 'et':
#                    print('\t', word, number)
