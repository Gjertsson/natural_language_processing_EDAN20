# -*- coding: utf-8 -*-
"""
Created on Sun Oct  9 11:40:24 2016

@author: Konrad Gjertsson och Karl Nilsson 
"""

import transition
import conll
from sklearn.externals import joblib
import features
import sys

def nivre_parse(formatted_corpus, classifier, feature_names, vec, classes):
    
    sent_cnt = 0
#    X_dict = list()
#    y_symbols = list()
    transitions = list()
    for sentence in formatted_corpus:
#        print(sentence)
#        print('################')
        sent_cnt += 1
        if sent_cnt % 1000 == 0:
            print(sent_cnt, 'sentences on', len(formatted_corpus), flush=True)
        stack = []
        queue = list(sentence)
        graph = {}
        graph['heads'] = {}
        graph['heads']['0'] = '0'
        graph['deprels'] = {}
        graph['deprels']['0'] = 'ROOT'
        
        while queue:
            feature = features.extract_third(stack, queue, graph, feature_names, sentence)
            
            zipped = dict(zip(feature_names, feature))
            
            x_list = []
            x_list.append(zipped)
            
            x = vec.transform(x_list)       
            trans_nr = classifier.predict(x)
    
#            print('trans_nr', trans_nr)            
            
            predicted_function = classes[trans_nr[0]]
#            print(predicted_function)
            
            stack, queue, graph, trans = parse_ml(stack, queue, graph, predicted_function)
            transitions.append(trans)
        stack, graph = transition.empty_stack(stack, graph)
#        print(sentence)
#        print(graph['heads'])
#        print(graph['deprels'])
#        print('############')

        for word in sentence:
            my_id = word['id']
            
#            print(my_id)
#            print(graph['heads'])
            
            word['head'] = graph['heads'][my_id]
            word['deprel'] = graph['deprels'][my_id]

        
#        print(sentence)
#        print('############hash-raden############')
    return transitions, formatted_corpus

def parse_ml(stack, queue, graph, trans):
    
    # right arc
    if stack and trans[:2] == 'ra':
        stack, queue, graph = transition.right_arc(stack, queue, graph, trans[3:])
        return stack, queue, graph, 'ra'
    
    # left arc
    if stack and trans[:2] == 'la':
        stack, queue, graph = transition.left_arc(stack, queue, graph, trans[3:])
        return stack, queue, graph, 'la'

    # reduce
    if stack and trans[:2] == 're':
        stack, queue, graph = transition.reduce(stack, queue, graph)
        return stack, queue, graph, 're'
        
    # shift
    stack, queue, graph = transition.shift(stack, queue, graph)
    return stack, queue, graph, 'sh'

if __name__ == "__main__":
    if len(sys.argv) == 3:
        vec = joblib.load(sys.argv[2])
        classes = joblib.load('classifiers/two/dict_two.pkl')
        
#        feature_names = ['word_stack', 'pos_stack', 'word_queue',
#                         'pos_queue', 'can_do_left', 'can_do_reduce']
        
#        feature_names = ['pos_stack_0', 'word_stack_0', 'pos_stack_1', 
#            'word_stack_1', 'pos_queue_0', 'word_queue_0', 'pos_queue_1', 
#            'word_queue_1', 'can_do_reduce', 'can_do_left']
        
        feature_names = ['pos_stack_0', 'word_stack_0', 'pos_stack_1', 
            'word_stack_1', 'pos_queue_0', 'word_queue_0', 'pos_queue_1', 
            'word_queue_1', 'can_do_reduce', 'can_do_left', 'sentence_word',
            'sentence_pos', 'sentence_word_n1', 'sentence_pos_n1']        
        
        train_file = 'data/swedish_talbanken05_train.conll'
        test_file = 'data/swedish_talbanken05_test_blind.conll'
        column_names_2006 = ['id', 'form', 'lemma', 'cpostag', 'postag', 'feats', 'head', 'deprel', 'phead', 'pdeprel']
        column_names_2006_test = ['id', 'form', 'lemma', 'cpostag', 'postag', 'feats']
        
        sentences = conll.read_sentences(test_file)
        formatted_corpus = conll.split_rows(sentences, column_names_2006_test)
        
        classifier = joblib.load(sys.argv[1])     
        
#        print(classes)        
        
#        print(classifier)
        transitions, corpus = nivre_parse(formatted_corpus, classifier, feature_names, vec, classes)
        
        conll.save('output.txt', corpus, column_names_2006)