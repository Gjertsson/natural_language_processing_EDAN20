# -*- coding: utf-8 -*-
"""
Created on Mon Oct  3 15:32:21 2016

@author: Konrad Gjertsson and Karl Nilsson
"""

import transition

def extract_first(stack, queue, state, feature_names, sentence):
    """
        feature_names = ['pos_stack', 'word_stack', 'pos_queue', 
        'word_queue', 'can_do_reduce', 'can_do_left']
    """
    word_queue = 'nil'
    word_stack = 'nil'    
    pos_stack = 'nil'
    pos_queue = 'nil'
    can_do_left = transition.can_leftarc(stack, state)
    can_do_reduce = transition.can_reduce(stack, state)
    
    if queue:
        word_queue = queue[0]['form']
        pos_queue = queue[0]['postag']
    if stack:
        word_stack = stack[0]['form']
        pos_stack = stack[0]['postag']
        
    return [pos_stack, word_stack, pos_queue, word_queue, 
            can_do_reduce, can_do_left]
    
def extract_second(stack, queue, state, feature_names, sentence):
    """
        feature_names = ['pos_stack_0', 'word_stack_0', 'pos_stack_1', 
        'word_stack_1', 'pos_queue_0', 'word_queue_0', 'pos_queue_1', 
        'word_queue_1', 'can_do_reduce', 'can_do_left']
    """
    
    word_queue_0 = 'nil'
    word_queue_1 = 'nil'
    word_stack_0 = 'nil'    
    word_stack_1 = 'nil'        
    
    pos_stack_0 = 'nil'
    pos_stack_1 = 'nil'
    pos_queue_0 = 'nil'
    pos_queue_1 = 'nil'
    
    can_do_left = transition.can_leftarc(stack, state)
    can_do_reduce = transition.can_reduce(stack, state)
    
    if queue:
        word_queue_0 = queue[0]['form']
        pos_queue_0 = queue[0]['postag']
        if len(queue) > 1:
            word_queue_1 = queue[1]['form']
            pos_queue_1 = queue[1]['postag']
    if stack:
        word_stack_0 = stack[0]['form']
        pos_stack_0 = stack[0]['postag']
        if len(stack) > 1:
            word_stack_1 = stack[1]['form']
            pos_stack_1 = stack[1]['postag']
            
#    return [pos_stack_0, word_stack_0, pos_stack_1, word_stack_1, 
#            pos_queue_0, word_queue_0, pos_queue_1, word_queue_1, 
#            can_do_reduce, can_do_left]
            
    return [pos_stack_0, pos_stack_1, word_stack_0, word_stack_1, 
        pos_queue_0, pos_queue_1, word_queue_0, word_queue_1, 
        can_do_reduce, can_do_left]
        
def extract_third(stack, queue, state, feature_names, sentence):
    """
        feature_names = ['pos_stack_0', 'word_stack_0', 'pos_stack_1', 
        'word_stack_1', 'pos_queue_0', 'word_queue_0', 'pos_queue_1', 
        'word_queue_1', 'can_do_reduce', 'can_do_left', 'sentence_word',
        'sentence_pos', 'sentence_word_n1', 'sentence_pos_n1']
    """
    
    word_queue_0 = 'nil'
    word_queue_1 = 'nil'
    word_stack_0 = 'nil'    
    word_stack_1 = 'nil'        
    
    pos_stack_0 = 'nil'
    pos_stack_1 = 'nil'
    pos_queue_0 = 'nil'
    pos_queue_1 = 'nil'
    
    sentence_word = 'nil'
    sentence_pos = 'nil'
    sentence_word_n1 = 'nil'
    sentence_pos_n1 = 'nil'
    
    can_do_left = transition.can_leftarc(stack, state)
    can_do_reduce = transition.can_reduce(stack, state)
    
    if queue:
        word_queue_0 = queue[0]['form']
        pos_queue_0 = queue[0]['postag']
        if len(queue) > 1:
            word_queue_1 = queue[1]['form']
            pos_queue_1 = queue[1]['postag']
    if stack:
        word_stack_0 = stack[0]['form']
        pos_stack_0 = stack[0]['postag']
    
        if len(stack) > 1:
            word_stack_1 = stack[1]['form']
            pos_stack_1 = stack[1]['postag']
            
        # new features
        index = 0
        for word in sentence:
            if word['form'] == stack[0]['form']:
                break
            else:
                index += 1
                
        if (index + 1) < len(sentence):
            sentence_word = sentence[index+1]['form']
            sentence_pos = sentence[index+1]['postag']
        
        if (index - 1) > -1:
            sentence_word_n1 = sentence[index-1]['form']
            sentence_pos_n1 = sentence[index-1]['postag']
    
    return [pos_stack_0, pos_stack_1, word_stack_0, word_stack_1, 
        pos_queue_0, pos_queue_1, word_queue_0, word_queue_1, 
        can_do_reduce, can_do_left, sentence_word, sentence_pos, 
        sentence_word_n1, sentence_pos_n1]
    
    