Kazakh UD treebank


BASIC STATISTICS

Tree count:  450
Word count:  5072
Token count: 4874
Dep. relations: 29 of which 1 language specific
POS tags: 16
Category=value feature pairs: 0


TOKENISATION

The tokenization in the Kazakh UD treebank follows the principles of [Turkic lexica in Apertium](http://wiki.apertium.org/wiki/Turkic_lexicon).


MORPHOLOGY

There is currently no morphological annotation in the Kazakh UD treebank.  This is planned for a future release.


DATA SETS

The treebank was randomly split into training (80%), testing (10%), and development (10%) sets.


CITATIONS

Please, cite the following papers if you use Kazakh UD treebank:

@inproceedings{tyers_tl2015,
  author = {Tyers, Francis M. and Washington, Jonathan N.},
  title = {Towards a Free/Open-source Universal-dependency Treebank for Kazakh},
  booktitle = {3rd International Conference on Turkic Languages Processing,
  (TurkLang 2015)},
  pages = {276--289},
  year = {2015},
}

@inproceedings{makazhan_tl2015,
  author = {Makazhanov, Aibek and
  Sultangazina, Aitolkyn and
  Makhambetov, Olzhas and
  Yessenbayev, Zhandos},
  title = {Syntactic Annotation of Kazakh: Following the Universal Dependencies Guidelines. A report},
  booktitle = {3rd International Conference on Turkic Languages Processing,
  (TurkLang 2015)},
  pages = {338--350},
  year = {2015},
}


--- Machine-readable metadata ---
Documentation status: partial
Data source: manual
Data available since: UD v1.3
License: CC BY-SA 4.0
Genre: wiki fiction
Contributors: Makazhanov, Aibek; Washington, Jonathan North; Tyers, Francis
