# UD_Estonian

The Estonian UD treebank is based on the [Estonian Dependency Treebank] (https://github.com/EstSyntax/) (EDT), created at the University of Tartu. The treebank consists of 234,351 274 tokens (punctuation included) and covers 3 different genres, namely newspaper texts, fiction and scientific texts.
The morphological and syntactic annotation of the Estonian UD treebank is created through automatic conversion of EDT data and as such, can contain errors.

Also, the previous release of Estonian UD Treebank which based on HamleDT 3.0 has been reannotated and included with the same tree identifiers to the treebank.

## Estonian Dependency Treebank

[EDT] (https://github.com/EstSyntax/EDT)
is a dependency oriented treebank released by the University of Tartu in 2014.

## Acknowledgments

We wish to thank all of the contributors to the original EDT annotation effort, especially Eleri Aedmaa, Riin Kirt and Dage Särg. Also, we wish to thank the authors of the previous treebank version for their effort.

## References

* Kadri Muischnek, Kaili Müürisep, Tiina Puolakainen, Eleri Aedmaa, Riin Kirt, Dage Särg.
  2014.
  [Estonian Dependency Treebank and its annotation scheme](http://tlt13.sfs.uni-tuebingen.de/tlt13-proceedings.pdf).
  In: *Proceedings of the 13th Workshop on Treebanks and Linguistic Theories (TLT13),*
  pp. 285–291, ISBN 978-3-9809183-9-8, Tübingen, Germany.

* Kadri Muischnek, Kaili Müürisep and Tiina Puolakainen 2016. Estonian Dependency Treebank: from Constraint Grammar tagset to Universal Dependencies. - Proceedings of LREC 2016.

## Changelog

* UD v1.2 contained Arborest, a much smaller and older VISL-style treebank. It has been re-annotated and added to EDT for UD v1.3.

=== Machine-readable metadata =================================================
Documentation status: stub
Data source: semi-automatic
Data available since: UD v1.2
License: CC BY-NC-SA 4.0
Genre: fiction news science
Contributors: Muischnek, Kadri; Müürisep, Kaili; Puolakainen, Tiina
===============================================================================
