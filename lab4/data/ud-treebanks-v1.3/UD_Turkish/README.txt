Turkish UD treebank

=== Machine-readable metadata =================================================
Documentation status: partial
Data source: automatic
Data available since: UD v1.3
License: CC BY-NC-SA
Genre: legal news
Contributors: Çöltekin, Çağrı; Cebiroğlu Eryiğit, Gülşen; Gokirmak, Memduh; Kaşıkara, Hüner; Sulubacak, Umut; Tyers, Francis
===============================================================================
