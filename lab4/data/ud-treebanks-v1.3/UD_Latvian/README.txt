# Treebank

Latvian UD Treebank is based on the newswire part of Latvian Treebank created at University of Latvia, Institute of Mathematics and Computer Science (http://sintakse.korpuss.lv).

The UD treebank consists of 1,082 sentences (20,105 tokens), and it has been obtained by automatic conversion of both the morphological and the syntactic annotations of the original treebank. The original data contains manually annotated syntax and semi-automatically annotated morphological tags and lemmas.

# Data sets

The training data contains news articles, interviews, annotations, media clippings and press releases. The development and test sets are split out to contain an interview, a news article, a press release, a clipping and few annotations.

Train: 673 sentences
Dev: 190 sentences
Test: 219 sentences

# Statictics

Tree count:  1082
Word count:  20106
Token count: 20106
Dep. relations: 29 of which 0 language specific
POS tags: 16
Category=value feature pairs: 50

# References

Pretkalniņa L., Rituma L. Constructions in Latvian Treebank: the Impact of Annotation Decisions on the Dependency Parsing Performance Proceedings of the 6th International Conference on Human Language Technologies — the Baltic Perspective (HLT 2014), Frontiers in Artificial Intelligence and Applications, Vol. 268, IOS Press, 2014, pp. 219–226

Pretkalniņa L., Nešpore G., Levāne-Petrova K., and Saulīte B. Towards a Latvian Treebank. Actas del 3 Congreso Internacional de Lingüística de Corpus. Tecnologias de la Información y las Comunicaciones: Presente y Futuro en el Análisis de Corpus, eds. Candel Mora M.Á., Carrió Pastor M., 2011, pp. 119–127

=== Machine-readable metadata =================================================
Documentation status: stub
Data source: automatic
Data available since: UD v1.3
License: CC BY-NC-SA 4.0
Genre: news
Contributors: Pretkalniņa, Lauma; Saulīte, Baiba; Rituma, Laura; Grūzītis, Normunds
===============================================================================
