"""
CoNLL-X and CoNLL-U file readers and writers
"""
__author__ = "Pierre Nugues"

import os, operator


def get_files(dir, suffix):
    """
    Returns all the files in a folder ending with suffix
    Recursive version
    :param dir:
    :param suffix:
    :return: the list of file names
    """
    files = []
    for file in os.listdir(dir):
        path = dir + '/' + file
        if os.path.isdir(path):
            files += get_files(path, suffix)
        elif os.path.isfile(path) and file.endswith(suffix):
            files.append(path)
    return files


def read_sentences(file):
    """
    Creates a list of sentences from the corpus
    Each sentence is a string
    :param file:
    :return:
    """
    f = open(file).read().strip()
    sentences = f.split('\n\n')
    return sentences


def split_rows(sentences, column_names):
    """
    Creates a list of sentence where each sentence is a list of lines
    Each line is a dictionary of columns
    :param sentences:
    :param column_names:
    :return:
    """
    new_sentences = []
    root_values = ['0', 'ROOT', 'ROOT', 'ROOT', 'ROOT', 'ROOT', '0', 'ROOT', '0', 'ROOT']
    start = [dict(zip(column_names, root_values))]
    for sentence in sentences:
        rows = sentence.split('\n')
        sentence = [dict(zip(column_names, row.split())) for row in rows if row[0] != '#']
        sentence = start + sentence
        new_sentences.append(sentence)
    return new_sentences


def save(file, formatted_corpus, column_names):
    f_out = open(file, 'w')
    for sentence in formatted_corpus:
        for row in sentence[1:]:
            # print(row, flush=True)
            for col in column_names[:-1]:
                if col in row:
                    f_out.write(row[col] + '\t')
                else:
                    f_out.write('_\t')
            col = column_names[-1]
            if col in row:
                f_out.write(row[col] + '\n')
            else:
                f_out.write('_\n')
        f_out.write('\n')
    f_out.close()
    
def generate_subject_verb_tuples(formatted_corpus):
    pairs = dict()    
    for sentence in formatted_corpus:  
        temp_dict = dict()
        for element in sentence:
            
            is_int = True            
            try:
                int(element['id'])
            except ValueError:
                is_int = False
            if is_int:
                temp_dict[element['id']] = element
                
        for key, element in temp_dict.items():        
            if element['deprel'] == 'nsubj':
                index = element['head']
                my_tuple = (element['form'].lower(), temp_dict[index]['form'].lower())
                if my_tuple in pairs:
                    pairs[my_tuple] += 1
                else:
                    pairs[my_tuple] = 1
    
    sorted_pairs = sorted(pairs.items(), key=operator.itemgetter(1), reverse=True)

    for i in range (0,5):
        print (sorted_pairs[i][0], sorted_pairs[i][1])
        
        
def generate_subject_verb_objects_tuples(formatted_corpus):
    triples = dict()    
    
    for sentence in formatted_corpus:
        temp_dict = dict()
        for element in sentence:
            
            is_int = True            
            try:
                int(element['id'])
            except ValueError:
                is_int = False
            if is_int:
                temp_dict[element['id']] = element
                
        for key, element in temp_dict.items():
            if element['deprel'] == 'nsubj': # SS before, nsubj now
                index = element['head']
                
                for key, second_element in temp_dict.items():
                    if second_element['deprel'] == 'dobj': # OO before, dobj now
                        if (int(index) == int(second_element['head'])):
                            my_triple = (element['form'].lower(), 
                                         temp_dict[index]['form'].lower(), 
                                            second_element['form'].lower())
                            if my_triple in triples:
                                triples[my_triple] += 1
                            else:
                                triples[my_triple] = 1
                        
    sorted_triples = sorted(triples.items(), key=operator.itemgetter(1), reverse=True)

    for i in range (0,5):
        print (sorted_triples[i][0], sorted_triples[i][1])

if __name__ == '__main__':
    column_names_2006 = ['id', 'form', 'lemma', 'cpostag', 'postag', 'feats', 'head', 'deprel', 'phead', 'pdeprel']

    train_file = 'data/ud-treebanks-v1.3/UD_Swedish/sv-ud-train.conllu'
#    train_file = 'data/swedish_talbanken05_train.conll'
    # train_file = 'test_x'
#    test_file = 'data/swedish_talbanken05_test.conll'

    sentences = read_sentences(train_file)
    formatted_corpus = split_rows(sentences, column_names_2006)
    
    
    generate_subject_verb_tuples(formatted_corpus)
    print('---------------')
    generate_subject_verb_objects_tuples(formatted_corpus)
    
    

#    column_names_u = ['id', 'form', 'lemma', 'upostag', 'xpostag', 'feats', 'head', 'deprel', 'deps', 'misc']
#
#    files = get_files('../../corpus/ud-treebanks-v1.3/', 'train.conllu')
#    for train_file in files:
#        sentences = read_sentences(train_file)
#        formatted_corpus = split_rows(sentences, column_names_u)
#        print(train_file, len(formatted_corpus))
#        print(formatted_corpus[0])
